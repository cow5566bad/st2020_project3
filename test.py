# -*- coding: utf-8 -*
import os
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	with open('window_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
		assert xmlString.find('首頁') != -1
	

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 1000 100')
	time.sleep(4)

# 1. [Content] Side Bar Text
def test_hasSidebarNode():
	os.system('adb shell input tap 100 100')
	time.sleep(4)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./sidebar_dump.xml')
	with open('sidebar_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
		testList = \
			['查看商品分類', '查訂單/退訂退款', '追蹤/買過/看過清單', '智慧標籤', 
			'PChome 旅遊', '線上手機回收', '給24h購物APP評分']
		for testItem in testList:
			assert xmlString.find(testItem) != -1

	os.system('adb shell input tap 1000 100')
	time.sleep(4)	

# 2. [Screenshot] Side Bar Text
def test_sidebar_screenshot():
	os.system('adb shell input tap 100 100')
	time.sleep(4)

	os.system('adb shell screencap -p /sdcard/sidebar.png && adb pull /sdcard/sidebar.png .')
	assert os.path.exists('sidebar.png')

	os.system('adb shell input tap 1000 100')
	time.sleep(4)

# 3. [Context] Categories
def test_categories():
	# scroll down
	os.system('adb shell input touchscreen swipe 500 1000 500 300 300')
	time.sleep(4)
	os.system('adb shell input tap 1000 640')
	time.sleep(4)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./categories_dump.xml')
	with open('categories_dump.xml') as f:
		xmlString = f.read()
		testList = \
			['精選', '3C', '周邊', 'NB',
			'通訊', '數位', '家電', '日用', 
			'食品', '生活', '運動戶外', '美妝', 
			'衣鞋包錶']
		for testItem in testList:
			assert xmlString.find(testItem) != -1

	# back to home page
	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)

# 4. [Screenshot] Categories
def test_categories_screenshot():
	# scroll down
	os.system('adb shell input touchscreen swipe 500 1000 500 300 300')
	time.sleep(4)
	os.system('adb shell input tap 1000 640')
	time.sleep(4)

	os.system('adb shell screencap -p /sdcard/categories.png && adb pull /sdcard/categories.png .')
	assert os.path.exists('categories.png')

	# back to home page
	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)

# 5. [Context] Categories page
def test_categories_page():
	os.system('adb shell input tap 330 1671')
	time.sleep(4)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./categories_page_dump.xml')
	with open('categories_page_dump.xml') as f:
		xmlString = f.read()
		testList = \
			['3C', '周邊', 'NB',
			'通訊', '數位', '家電', 
			'日用', '食品', '生活', 
			'運動戶外', '美妝', '衣鞋包錶']
		for testItem in testList:
			assert xmlString.find(testItem) != -1

	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)

# 6. [Screenshot] Categories page
def test_categories_page_screenshot():
	os.system('adb shell input tap 330 1671')
	time.sleep(4)

	os.system('adb shell screencap -p /sdcard/categories_page.png && adb pull /sdcard/categories_page.png .')
	assert os.path.exists('categories_page.png')

	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)

# 7. [Behavior] Search item “switch”
def test_search():
	os.system('adb shell input tap 700 100')
	time.sleep(4)
	os.system('adb shell input text "switch"')
	time.sleep(4)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(4)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./search_switch_dump.xml')
	with open('search_switch_dump.xml') as f:
		xmlString = f.read()
		assert xmlString.find("Switch") != -1 

# 8. [Behavior] Follow an item and it should be add to the list
	os.system('adb shell input tap 500 500')
	time.sleep(5)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 120 886')
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 500 700 500 1400 300')
	time.sleep(4)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./follow_switch_dump.xml')
	with open('follow_switch_dump.xml') as f:
		xmlString = f.read()
		assert xmlString.find("Switch") != -1 

# 9. [Behavior] Navigate to the detail of item
	os.system('adb shell input tap 120 886')
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 500 1000 500 300 300')
	time.sleep(4)
	os.system('adb shell input tap 540 100')
	time.sleep(4)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./detail_switch_dump.xml')
	with open('detail_switch_dump.xml') as f:
		xmlString = f.read()
		testList = ['詳細資訊']
		for testItem in testList:
			assert xmlString.find(testItem) != -1
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 100 100')
	time.sleep(4)
	os.system('adb shell input tap 100 1671')
	time.sleep(4)

# 10. [Screenshot] Disconnetion Screen
def test_disconnection():
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 540 25 540 300 300')
	time.sleep(4)
	os.system('adb shell input tap 972 295')
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 540 1777 540 1377 300')
	time.sleep(4)
	os.system('adb shell input tap 540 1671')
	time.sleep(4)

	os.system('adb shell screencap -p /sdcard/disconnect.png && adb pull /sdcard/disconnect.png .')
	assert os.path.exists('disconnect.png')

	os.system('adb shell input tap 100 1671')
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 540 25 540 300 300')
	time.sleep(4)
	os.system('adb shell input tap 972 295')
	time.sleep(4)
	os.system('adb shell input touchscreen swipe 540 1777 540 1377 300')
	time.sleep(4)
